package urma.chap2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Adam on 7/15/2015.
 */
public class InterVsTerminal {

    public static void main(String[] args) {

        List<Person> sample = Arrays.asList(new Person(280, "usa"),
                new Person(189, "china"),
                new Person(155, "russia"),
                new Person(125, "canada"),
                new Person(120, "china"),
                new Person(185, "russia"),
                new Person(155, "china"),
                new Person(195, "usa"),
                new Person(155, "russia"),
                new Person(255, "usa"),
                new Person(155, "usa"),
                new Person(200, "china"));


        System.out.println("&&&&&&&&&&&&&&& first loop &&&&&&&&&&&&&&&&&&&&&");

        int[] idx = { 0 };

        sample.stream()

                .filter(s -> {  //intermediate operation
                    System.out.println(++idx[0] + ": filter: " + s);
                    return s.getOrigin().equals("usa");
                })
                .sorted((s1, s2) -> {  //intermediate operation
                    System.out.printf(++idx[0] + ": sort: %s; %s\n", s1, s2);
                    return s1.getIq().compareTo(s2.getIq());
                })

                .map(s -> {  //intermediate operation
                    System.out.println(++idx[0] + ": map: " + s);
                    return s.getIq() + " : "+ s.getOrigin().toUpperCase();
                })

                //toggle this on (and remove semicolone above) to see the difference
                .forEach(s -> System.out.println(++idx[0] + ": forEach: " + s));  //terminal opeeration


    }





    //our POJO
    public static class Person  {
        private int iq = 0;
        private String origin = "";

        public Person(int iq, String origin) {
            this.iq = iq;
            this.origin = origin;
        }

        public Integer getIq() {
            return iq;
        }

        public void setIq(Integer iq) {
            this.iq = iq;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String toString() {
            return "Person{" +
                    "origin='" + origin + '\'' +
                    ", iq=" + iq +
                    '}';
        }
    }

}
